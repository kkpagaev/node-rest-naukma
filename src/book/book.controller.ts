import { BookService } from "./book.service"
import { Request, Response } from "express"
import { CreateBookDto } from "../dto/create-book.dto"
import { decodeJWT } from "../helpers"

export class BookController {
  constructor(private bookService: BookService) {}

  async getBooks(req: Request, res: Response): Promise<void> {
    const limit = +req.query.limit || 10
    const page = +req.query.page || 1
    const books = await this.bookService.paginate(page, limit)

    res.status(200).send(books)
  }

  async createBook(req: Request, res: Response) {
    const { user_id } = decodeJWT(req)
    const dto = new CreateBookDto()
    dto.title = req.body.title
    dto.description = req.body.description
    const book = await this.bookService.createBook(+user_id, dto)

    res.status(201).send(book)
  }

  async getBook(req: Request, res: Response) {
    const id = +req.params.id
    const book = await this.bookService.findById(id)

    res.status(200).send(book)
  }

  async updateBook(req: Request, res: Response) {
    const { user_id } = decodeJWT(req)
    const id = +req.params.id
    const dto = new CreateBookDto()
    dto.title = req.body.title
    dto.description = req.body.description
    const book = await this.bookService.updateBook(id, user_id, dto)

    res.status(200).send(book)
  }

  async deleteBook(req: Request, res: Response) {
    const { user_id } = decodeJWT(req)
    const id = +req.params.id
    await this.bookService.deleteBook(id, user_id)

    res.status(204).send()
  }
}
