import NotFoundException from "../core/exceptions/not-found.exception"
import { getTestDataSource } from "../test.utils"
import { BookService } from "./book.service"
import { Book } from "./entities/book.entity"

describe("BooksService", () => {
  let booksService: BookService

  beforeEach(async () => {
    const dataSource = await getTestDataSource()
    const bookRepository = dataSource.getRepository(Book)
    booksService = new BookService(bookRepository)
  })

  it("should be defined", () => {
    expect(booksService).toBeDefined()
  })

  it("should get created book", async () => {
    const book = await booksService.createBook({
      title: "Test",
      description: "Test",
    })
    const foundBook = await booksService.findById(book.id)
    expect(foundBook).toBeDefined()
  })

  it("should create a book", async () => {
    const book = await booksService.createBook({
      title: "Old title",
      description: "Old description",
    })
    expect(book).toBeDefined()
  })

  it("should update a book", async () => {
    const book = await booksService.createBook({
      title: "Test",
      description: "Test",
    })
    const updatedBook = await booksService.updateBook(book.id, {
      title: "Updated title",
      description: "Updated description",
    })
    expect(book.id === updatedBook.id).toBeTruthy()
    const foundBook = await booksService.findById(book.id)
    expect(foundBook.title).toEqual("Updated title")
  })

  it("should delete a book", async () => {
    const book = await booksService.createBook({
      title: "Test",
      description: "Test",
    })
    await booksService.deleteBook(book.id)
    await expect(booksService.findById(book.id)).rejects.toThrowError(
      NotFoundException
    )
  })

  it("should paginate books", async () => {
    for (let i = 0; i < 20; i++) {
      await booksService.createBook({
        title: "Book",
        description: "Test",
      })
    }
    const books1 = await booksService.paginate(1, 10)
    const books2 = await booksService.paginate(2, 10)
    expect(books1.length).toEqual(10)
    expect(books2.length).toEqual(10)
    expect(books1[0].id !== books2[0].id).toBeTruthy()
  })
})
