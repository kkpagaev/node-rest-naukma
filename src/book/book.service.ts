import { CreateBookDto } from "src/dto/create-book.dto"
import { UpdateBookDto } from "src/dto/update-book.dto"
import { Repository } from "typeorm"
import { Service } from "../core/base-service"
import NotFoundException from "../core/exceptions/not-found.exception"
import UnauthorizedException from "../core/exceptions/unauthorized.exception"
import { Book } from "./entities/book.entity"

export interface PaginationResponse<T> {
  data: T[]
  page: number
  limit: number
  total: number
}

export class BookService extends Service {
  constructor(private bookRepository: Repository<Book>) {
    super()
  }

  async paginate(
    page: number,
    limit: number
  ): Promise<PaginationResponse<Book>> {
    const books = await this.bookRepository.find({
      skip: (page - 1) * limit,
      take: limit,
      relations: ["user"],
    })

    const total = await this.bookRepository.count()

    return {
      data: books,
      page,
      limit,
      total,
    } as PaginationResponse<Book>
  }

  async getBooks(): Promise<Book[]> {
    const books = await this.bookRepository.find()

    return books
  }

  async createBook(user_id: number, dto: CreateBookDto): Promise<Book> {
    await this.validate(dto)
    const book = this.bookRepository.create({
      title: dto.title,
      description: dto.description,
      userId: user_id,
    })
    await this.bookRepository.save(book)

    return book
  }

  async updateBook(id: number, user_id: number, dto: UpdateBookDto) {
    await this.validate(dto)
    const book = await this.findById(id)
    if (book.userId !== user_id) {
      throw new UnauthorizedException()
    }
    return await this.bookRepository.save({
      id: book.id,
      ...dto,
    })
  }

  async findById(id: number) {
    const book = await this.bookRepository.findOne({
      where: {
        id,
      },
    })
    if (!book) {
      throw new NotFoundException()
    }

    return book
  }

  async deleteBook(id: number, user_id: number) {
    const book = await this.findById(id)
    if (book.userId !== user_id) {
      throw new UnauthorizedException()
    }
    await this.bookRepository.remove(book)
  }
}
