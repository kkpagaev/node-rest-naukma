import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"
import { User } from "../../user/user.entity"

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  title: string

  @Column()
  description: string

  @Column({ name: "userId", nullable: true })
  userId: number

  @ManyToOne(() => User, (user) => user.books, {
    onDelete: "CASCADE",
  })
  public user: User
}
