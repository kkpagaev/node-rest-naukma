import { DataSource } from "typeorm"
import { Book } from "./book/entities/book.entity"
import { DBConfig } from "./core/config/db.config"
import { User } from "./user/user.entity"

export const createDataSource = (cfg: DBConfig): DataSource => {
  const entities = [Book, User]

  const dataSource = new DataSource({
    type: "mysql",
    host: cfg.host,
    port: cfg.port,
    username: cfg.username,
    password: cfg.password,
    database: cfg.database,
    entities: entities,
    logging: process.env.NODE_ENV === "development",
    synchronize: true,
  })

  return dataSource
}
