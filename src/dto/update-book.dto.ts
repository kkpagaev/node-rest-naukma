import { IsString, IsNotEmpty } from "class-validator"

export class UpdateBookDto {
  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @IsNotEmpty()
  description: string
}
