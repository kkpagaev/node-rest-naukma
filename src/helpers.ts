import { decode, verify } from "jsonwebtoken"
import UnauthorizedException from "./core/exceptions/unauthorized.exception"
import { JWTPayload } from "./user/user.service"
import { Request } from "express"

export function decodeJWT(req: Request) {
  if (!req.headers.authorization) {
    throw new UnauthorizedException()
  }
  const token = req.headers.authorization.replace("Bearer ", "")
  if (!verify(token, process.env.JWT_SECRET || "")) {
    throw new UnauthorizedException()
  }
  const res = decode(token, {
    json: true,
  }) as JWTPayload

  return res
}
