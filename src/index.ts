import * as dotenv from "dotenv"
import * as express from "express"
import { Application } from "express"
import helmet from "helmet"
import { BookController } from "./book/book.controller"
import { BookService } from "./book/book.service"
import { Book } from "./book/entities/book.entity"
import { createDataSource } from "./data-source"
import { Request, Response } from "express"
import { json, urlencoded } from "body-parser"
import { Router } from "./router"
import { getConfig } from "./core/config/config"
import { User } from "./user/user.entity"
import { UserService } from "./user/user.service"
import { UserController } from "./user/user.controller"

async function main() {
  dotenv.config()
  const app: Application = express()
  const port = process.env.PORT || 3000
  app.use(
    urlencoded({
      extended: true,
    })
  )

  app.use(json())
  app.use(helmet())

  const config = getConfig()
  // typeorm
  const dataSource = createDataSource(config.db)
  await dataSource.initialize()

  // books
  const bookRepository = dataSource.getRepository(Book)
  const bookService = new BookService(bookRepository)
  const bookController = new BookController(bookService)

  // users
  const userRepository = dataSource.getRepository(User)
  const userService = new UserService(userRepository)
  const userController = new UserController(userService)

  // router
  const router = new Router(app)

  router.get("/books", async (req: Request, res: Response) => {
    await bookController.getBooks(req, res)
  })

  router.post("/books", async (req: Request, res: Response) => {
    await bookController.createBook(req, res)
  })

  router.get("/books/:id", async (req: Request, res: Response) => {
    await bookController.getBook(req, res)
  })

  router.put("/books/:id", async (req: Request, res: Response) => {
    await bookController.updateBook(req, res)
  })

  router.delete("/books/:id", async (req: Request, res: Response) => {
    await bookController.deleteBook(req, res)
  })

  router.post("/users", async (req: Request, res: Response) => {
    await userController.createUser(req, res)
  })

  router.post("/auth/signin", async (req: Request, res: Response) => {
    await userController.signin(req, res)
  })

  app.listen(port, () => console.log(`Server is listening on port ${port}!`))
}
/*eslint-disable*/
main().catch((err) => console.error(err))
/*eslint-enable*/
