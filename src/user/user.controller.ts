import { Request, Response } from "express"
import { CreateUserDto } from "./dto/create-user.dto"
import { SigninDto } from "./dto/signin.dto"
import { UserService } from "./user.service"
import { decode } from "jsonwebtoken"

export class UserController {
  constructor(private service: UserService) {}

  async createUser(req: Request, res: Response) {
    const dto = new CreateUserDto()
    dto.email = req.body.email
    dto.password = req.body.password
    dto.name = req.body.name
    const user = await this.service.createUser(dto)

    res.status(201).send(user)
  }

  async signin(req: Request, res: Response) {
    const dto = new SigninDto()
    dto.email = req.body.email
    dto.password = req.body.password
    const jwt = await this.service.signin(dto)

    res.status(200).send({
      access_token: jwt,
      payload: decode(jwt, { json: true }),
    })
  }
}
