import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { Book } from "../book/entities/book.entity"
import { compareSync } from "bcrypt"

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column({
    unique: true,
  })
  email: string

  @Column()
  password: string

  @OneToMany(() => Book, (book) => book.user)
  public books: Book[]

  comparePassword(password: string) {
    return compareSync(password, this.password)
  }
}
