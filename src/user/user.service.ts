import { Repository } from "typeorm"
import { Service } from "../core/base-service"
import NotFoundException from "../core/exceptions/not-found.exception"
import { CreateUserDto } from "./dto/create-user.dto"
import { User } from "./user.entity"
import { hashSync } from "bcrypt"
import { SigninDto } from "./dto/signin.dto"
import { sign } from "jsonwebtoken"

export interface JWTPayload {
  user_id: number
  email: string
  name: string
}

export class UserService extends Service {
  constructor(private repo: Repository<User>) {
    super()
  }

  async createUser(dto: CreateUserDto) {
    await this.validate(dto)
    const hash = hashSync(dto.password, 10)
    const user = this.repo.create({
      email: dto.email,
      password: hash,
      name: dto.name,
    })
    await this.repo.save(user)

    return user
  }

  async findByEmail(email: string) {
    const user = await this.repo.findOne({
      where: {
        email,
      },
    })
    if (!user) {
      throw new NotFoundException()
    }

    return user
  }

  async signin(dto: SigninDto) {
    await this.validate(dto)
    const user = await this.findByEmail(dto.email)
    if (!user) {
      throw new NotFoundException()
    }
    if (!user.comparePassword(dto.password)) {
      throw new NotFoundException()
    }

    return this.generateJwt(user)
  }

  private generateJwt(user: User) {
    const payload = {
      user_id: user.id,
      email: user.email,
      name: user.name,
    } as JWTPayload

    return sign(payload, process.env.JWT_SECRET, {
      expiresIn: "1h",
    })
  }
}
